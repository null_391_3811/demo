package com.woniu.vo;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class Node {

    private Integer Id;

    private String name;

    private Integer pid;


    private List<Node> treeNode = new ArrayList<>();

    public Node(int id, int pid) {
        this.Id = id;
        this.pid = pid;
    }

    public Node(int id, int pid, String name) {
        this(id, pid);
        this.name = name;
    }
}

