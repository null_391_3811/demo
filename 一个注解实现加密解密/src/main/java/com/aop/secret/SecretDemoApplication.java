package com.aop.secret;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecretDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecretDemoApplication.class, args);
	}

}
