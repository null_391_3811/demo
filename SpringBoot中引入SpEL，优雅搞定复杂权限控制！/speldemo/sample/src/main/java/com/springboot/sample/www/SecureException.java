package com.springboot.sample.www;

public class SecureException extends RuntimeException {

    private String code;

    public SecureException(String code) {
        this.code = code;
    }

    public SecureException() {

    }
}
