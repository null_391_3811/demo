package com.woniu.demo.controller;


import com.woniu.demo.model.request.UserReq;
import com.woniu.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 接口如何防抖(防重复提交)？可以这样做！
     * 1 我们需要给这两次接口的调用加一个时间间隔，大于这个时间间隔的一定不是重复提交；
     * 2 两次请求提交的参数比对，不一定要全部参数，选择标识性强的参数即可；
     * 3 如果想做的更好一点，还可以加一个请求地址的对比。
     */
    @PostMapping("/add")
    public ResponseEntity<String> addUser(@RequestBody UserReq addReq) {
        return userService.addUser(addReq);
    }


}
