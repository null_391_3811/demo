package com.woniu.demo.model.request;


import com.woniu.demo.annotation.RequestKeyParam;
import lombok.Data;

@Data
public class UserReq {

    /**
     * 用户名称
     */
    @RequestKeyParam
    private String userName;

    /**
     * 用户手机号
     */
    @RequestKeyParam
    private String userPhone;


}
