package com.woniu.commonswitch.constant;

/**
 * <p>
 * 常量类
 * 程序员蜗牛
 * </p>
 *
 */
public class Constant {

    /**
     * (0:关，1:开)
     */
    public static final String SWITCHCLOSE =  "0";

    // .... 其他业务相关的常量 ....

    // 配置相关的常量
    public static class ConfigCode {

        // 挂号支付开关(0:关，1:开)
        public static final String REG_PAY_SWITCH = "reg_pay_switch";
        // 门诊支付开关(0:关，1:开)
        public static final String CLINIC_PAY_SWITCH = "clinic_pay_switch";

        // 其他业务相关的配置常量
        // ....
    }
}
