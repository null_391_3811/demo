package org.example.maptovalue;


import com.google.common.collect.Lists;
import org.example.collectiontomap.CollectionToMap;
import org.example.entity.OrderItem;
import org.junit.Test;

import java.util.List;
import java.util.Map;

/**
 * Map格式转换
 * 转换 Map 的 Value
 * 将 Map<Long, OrderItem> 中的value 转化为 Map<Long, Double>
 * value 转化时，lamada表达式可以使用（v)->{}， 也可以使用 （k，v）->{ }。
 */
public class TestMapToValue {

    @Test
    public void testConvertValue() {
        List<OrderItem> orderItems = Lists.newArrayList(
                new OrderItem(1, 5d, "手表"),
                new OrderItem(2, 6d, "机器人"),
                new OrderItem(3, 8d, "手机")
        );
        Map<Integer, OrderItem> map = CollectionToMap.toMap(orderItems, OrderItem::getOrderId);
        Map<Integer, String> convertMap = MapToValue.convertMapValue(map, (id, item) -> id + item.getName());

    }

}
