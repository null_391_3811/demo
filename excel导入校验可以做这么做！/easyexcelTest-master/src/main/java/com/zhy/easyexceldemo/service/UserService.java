package com.zhy.easyexceldemo.service;

import com.zhy.easyexceldemo.dto.UserExcelDto;
import com.zhy.easyexceldemo.easyexcel.ExcelCheckManager;

/**
 * @author woniu
 */
public interface UserService extends ExcelCheckManager<UserExcelDto> {
}
