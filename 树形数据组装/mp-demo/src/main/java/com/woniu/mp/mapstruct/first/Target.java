package com.woniu.mp.mapstruct.first;

import lombok.Data;

import java.util.Date;


@Data
public class Target {
 private String stringProperty;
 private long longProperty;
 private String stringConstant;
 private Integer integerConstant;
 private Long  longWrapperConstant;
 private Date dateConstant;

}
