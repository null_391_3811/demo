package com.woniu.mp.mapstruct.third;

import lombok.Data;

import java.util.Date;

@Data
public class BikeDto {

 /**
  * 唯一id
  */
 private String id;

 private Date creationDate;

 /**
  * 品牌
  */
 private String brand;
}
